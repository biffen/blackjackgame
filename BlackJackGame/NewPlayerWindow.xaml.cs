﻿/**
  Project: BlackJackGame 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-09-22
*/

using System;
using System.Windows;

namespace BlackJackGame
{
    /// <summary>
    /// Interaction logic for NewPlayerWindow.xaml
    /// Small window that is used to enter information about new players.
    /// </summary>
    public partial class NewPlayerWindow : Window
    {
        public NewPlayerWindow()
        {
            InitializeComponent();
            tbName.Focus();
            tbName.Select(0,0);
        }

        /// <summary>
        /// Method for the Add button. Sets the DialogResult to true.
        /// </summary>
        private void btnAdd_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = true;
        }

        /// <summary>
        /// The content of the Name textbox
        /// </summary>
        public string PlayerName
        {
            get { return tbName.Text; }
        }

        /// <summary>
        /// The amount of money entered, or 0 if the amount
        /// could not be parsed.
        /// </summary>
        public int PlayerMoney
        {
            get { return GetMoney(); }
        }

        /// <summary>
        /// Attempts to parse the entered amount of money and returns it.
        /// If the entered string is not parseable it will return 0.
        /// </summary>
        /// <returns>The parsed integer or 0.</returns>
        private int GetMoney()
        {
            int money = 0;
            Int32.TryParse(tbMoney.Text, out money);
            return money;
        }
    }
}
