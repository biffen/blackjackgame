﻿/**
  Project: BlackJackGame 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-10-04
*/

using System;
using System.Windows;
using System.Windows.Controls;
using System.Windows.Media;
using System.Windows.Media.Animation;
using System.Windows.Media.Imaging;
using System.Collections.Generic;
using System.Diagnostics;
using BlackJackLogic;

namespace BlackJackGame
{
    /// <summary>
    /// Interaction logic for MainWindow.xaml
    /// The main window of the project.
    /// Handles all graphical elements and user input.
    /// </summary>
    public partial class MainWindow : Window
    {
        // Some useful UI constants
        private const int DealerId = 0;
        private const int CardWidth = 71;
        private const int CardHeight = 96;
        private const int DealerFirstCardLeft = 414;
        private const int DealerFirstCardTop = 63;
        private const int CardSpacing = 30;
        private const int PlayerControlTop = 270;

        // How long the cards should animate/mvoe
        private static readonly TimeSpan DefaultAnimationTime = TimeSpan.FromSeconds(1);
        
        private GameEngine _gameEngine;
        private List<PlayerControl> _playerControls;
        private List<Image> _cardImages;
        private PlayerControl _currentPlayerControl;

        public MainWindow()
        {
            InitializeComponent();
            InitializeGame();
        }

        /// <summary>
        /// Initializes the needed variables 
        /// </summary>
        private void InitializeGame()
        {
            _gameEngine = new GameEngine(2);
            _playerControls = new List<PlayerControl>();
            _cardImages = new List<Image>();
            lblDealer.Content = _gameEngine.Dealer.Name;

            foreach (Player player in _gameEngine.Players)
            {
                AddPlayerControl(player);
            }
        }

        /// <summary>
        /// Creates a player object with the given information and adds a 
        /// playercontrol for the newly created player.
        /// </summary>
        /// <param name="playerName">The name of the new player</param>
        /// <param name="playerMoney">The money that the player starts with</param>
        private void AddPlayer(string playerName, int playerMoney)
        {
            Player newPlayer = _gameEngine.AddPlayer(playerName, playerMoney);
            AddPlayerControl(newPlayer);
        }

        /// <summary>
        /// Removes the player from the GameEngine, database and the UI.
        /// </summary>
        /// <param name="player">The player that should be removed.</param>
        private void RemovePlayer(Player player)
        {
            for (int i = 0; i < _playerControls.Count; i++)
            {
                if (_playerControls[i].PlayerId == player.PlayerId)
                {
                    if (_gameEngine.RemovePlayer(_playerControls[i].Player))
                    {
                        MainCanvas.Children.Remove(_playerControls[i]);
                        _playerControls.RemoveAt(i);
                        return;
                    }
                }
            }
            Debug.WriteLine("Player could not be removed.");
        }

        /// <summary>
        /// Creates a PlayerControl for the given player and adds it to the UI.
        /// </summary>
        /// <param name="newPlayer">The player the PlayerControl will belong to.</param>
        private void AddPlayerControl(Player newPlayer)
        {
            PlayerControl newPlayerControl = new PlayerControl(newPlayer);
            newPlayerControl.BetOneEvent += PlayerBetOne;
            newPlayerControl.BetFiveEvent += PlayerBetFive;
            newPlayerControl.BetTenEvent += PlayerBetTen;
            newPlayerControl.BetTwentyFiveEvent += PlayerBetTwentyFive;
            newPlayerControl.FinishedBettingEvent += PlayerFinishedBetting;
            newPlayerControl.StandEvent += PlayerStand;
            newPlayerControl.HitMeEvent += PlayerHit;
            MainCanvas.Children.Add(newPlayerControl);

            Canvas.SetLeft(newPlayerControl, 20 + (330*_playerControls.Count));
            Canvas.SetTop(newPlayerControl, PlayerControlTop);

            _playerControls.Add(newPlayerControl);

            btnStartNewGame.IsEnabled = true;
        }

        /// <summary>
        /// Draws a new card for the player with the given ID.
        /// </summary>
        /// <param name="playerId">The ID of the player that wants a card.</param>
        private void DrawNewCard(int playerId)
        {
            // Draw a new card
            Card newCard = _gameEngine.Deck.DrawNextCard();

            // Create a image object for the new card
            Image newCardImage = new Image
            {
                Width = CardWidth,
                Height = CardHeight,
                Source = GetCardImage(newCard)
            };

            // Add the image to the canvas
            MainCanvas.Children.Add(newCardImage);

            // Set a starting pos
            Canvas.SetLeft(newCardImage, Canvas.GetLeft(imgCardStack));
            Canvas.SetTop(newCardImage, Canvas.GetTop(imgCardStack));

            int numOfCards;
            int cardTop;
            int cardLeft;
            if (playerId == 0) // Dealer
            {
                numOfCards = _gameEngine.Dealer.Hand.NumberOfCards;
                cardTop = DealerFirstCardTop;
                _gameEngine.Dealer.Hand.AddCard(newCard);
                cardLeft = DealerFirstCardLeft + (numOfCards * CardSpacing);
            }
            else // Normal player
            {
                PlayerControl currentPlayerControl = null;
                foreach (PlayerControl playerControl in _playerControls)
                {
                    if (playerControl.PlayerId == playerId)
                    {
                        currentPlayerControl = playerControl;
                    }
                }

                if (currentPlayerControl == null)
                {
                    MessageBox.Show("The player ID was not found when drawing card.");
                    return;
                }

                numOfCards = currentPlayerControl.Player.Hand.NumberOfCards;
                currentPlayerControl.Player.Hand.AddCard(newCard);
                cardTop = (int) Canvas.GetTop(currentPlayerControl) + 50;
                cardLeft = ((int) Canvas.GetLeft(currentPlayerControl)) + (numOfCards * CardSpacing);
            }

            // Add the image to the List for easy removal later
            _cardImages.Add(newCardImage);

            // Moves/animates the card to its location
            MoveImage(newCardImage, cardLeft, cardTop);

            // Update the score labels
            UpdateScores();
        }

        /// <summary>
        /// Updates the scores of the dealer and players
        /// </summary>
        private void UpdateScores()
        {
            foreach (PlayerControl playerControl in _playerControls)
            {
                playerControl.UpdateScore();
            }
            lblDealerScore.Content = _gameEngine.Dealer.Hand.Score;
        }

        /// <summary>
        /// Updates the players money
        /// </summary>
        private void UpdateMoney()
        {
            foreach (PlayerControl playerControl in _playerControls)
            {
                playerControl.UpdateMoney();
            }
        }

        /// <summary>
        /// Places bet
        /// </summary>
        /// <param name="bettingPlayer">The player that wish to place a bet.</param>
        /// <param name="betValue">The value to bet</param>
        private void Bet(Player bettingPlayer, int betValue)
        {
            foreach (Player player in _gameEngine.Players)
            {
                if (player.PlayerId == bettingPlayer.PlayerId)
                {
                    if (betValue > player.Money)
                    {
                        MessageBox.Show("You dont have enough money for that bet.");
                        return;
                    }
                    player.Bet(betValue);
                    UpdateMoney();
                    return;
                }
            }
            MessageBox.Show("Could not find player with ID:" + bettingPlayer.PlayerId);
        }

        /// <summary>
        /// Moves the given image to the new position with a animation. Animation time
        /// is set in DEFAULT_ANIMATION_TIME.
        /// Source: http://stackoverflow.com/questions/4214155/wpf-easiest-way-to-move-image-to-x-y-programmatically
        /// </summary>
        /// <param name="img">The image that should be moved.</param>
        /// <param name="newPosX">New position in X space.</param>
        /// <param name="newPosY">New position in Y space.</param>
        private static void MoveImage(Image img, double newPosX, double newPosY)
        {
            double oldPosX = Canvas.GetLeft(img);
            double oldPosY = Canvas.GetTop(img);

            TranslateTransform translateTransform = new TranslateTransform();
            img.RenderTransform = translateTransform;

            // Animate from original pos (0) to new pos
            DoubleAnimation animateX = new DoubleAnimation(0, newPosX - oldPosX, DefaultAnimationTime);
            DoubleAnimation animateY = new DoubleAnimation(0, newPosY - oldPosY, DefaultAnimationTime);

            translateTransform.BeginAnimation(TranslateTransform.XProperty, animateX);
            translateTransform.BeginAnimation(TranslateTransform.YProperty, animateY);
        }

        /// <summary>
        /// Uses the information in the given Card object to create a BitmapImage
        /// object with the correct image.
        /// </summary>
        /// <param name="card">The card.</param>
        /// <returns>The BitmapImage of the card.</returns>
        private BitmapImage GetCardImage(Card card)
        {
            string fileName = string.Empty;

            if (card.Suite == Card.Suits.Hearts)
            {
                fileName += "h";
            }
            else if (card.Suite == Card.Suits.Clubs)
            {
                fileName += "c";
            }
            else if (card.Suite == Card.Suits.Diamonds)
            {
                fileName += "d";
            }
            else if (card.Suite == Card.Suits.Spades)
            {
                fileName += "s";
            }

            if (card.Value == 11)
            {
                fileName += "j";
            }
            else if (card.Value == 12)
            {
                fileName += "q";
            }
            else if (card.Value == 13)
            {
                fileName += "k";
            }
            else
            {
                fileName += card.Value.ToString();
            }

            fileName += ".png";

            return new BitmapImage(new Uri(@"/BlackJackGame;component/Images/Cards/" + fileName, UriKind.Relative));
        }

        /// <summary>
        /// Method that is used when a "finished betting" event is fired
        /// </summary>
        /// <param name="sender">The PlayerControl that fired the event.</param>
        /// <param name="e"></param>
        private void PlayerFinishedBetting(object sender, RoutedEventArgs e)
        {
            bool allFinished = true;
            foreach (PlayerControl playerControl in _playerControls)
            {
                if (!playerControl.Player.IsDoneBetting && playerControl.PlayerId > 0)
                {
                    allFinished = false;
                }
            }

            if (allFinished)
            {
                DrawNewCard(DealerId);
                foreach (PlayerControl playerControl in _playerControls)
                {
                    DrawNewCard(playerControl.PlayerId);
                    DrawNewCard(playerControl.PlayerId);
                }
                SetCurrentPlayer(_playerControls[0]);
            } 
        }

        /// <summary>
        /// Method that is used when a "hit me" event is fired from a PlayerControl.
        /// </summary>
        /// <param name="sender">The PlayerControl that fired the event.</param>
        /// <param name="e"></param>
        private void PlayerHit(object sender, RoutedEventArgs e)
        {
            DrawNewCard(((PlayerControl) sender).PlayerId);
        }

        /// <summary>
        /// Sets a PlayerControl to be the current player.
        /// </summary>
        /// <param name="playerControl">The PlayerControl that belong to the player.</param>
        private void SetCurrentPlayer(PlayerControl playerControl)
        {
            _currentPlayerControl = playerControl;
            _currentPlayerControl.SetCurrentPlayer();
        }

        /// <summary>
        /// Method that is used when a "Stand" event is fired from a PlayerControl.
        /// If there are more players in the list, the next one will be set as the 
        /// current player.
        /// If there are not any more players in the list the dealer will deal out 
        /// cards to himself untill he is done.
        /// The game will then be evaluated and money given to the winners aswell
        /// as the labels with money updated.
        /// When the round is done the "Add player" option in the menu will be enabled
        /// and the "Start game" button enabled.
        /// </summary>
        /// <param name="sender">The PlayerControl that fired the event.</param>
        /// <param name="e"></param>
        private void PlayerStand(object sender, RoutedEventArgs e)
        {
            int currentControlIndex = _playerControls.IndexOf(_currentPlayerControl);
            if (_playerControls.Count - 1 > currentControlIndex)
            {
                SetCurrentPlayer(_playerControls[currentControlIndex + 1]);
            }
            else
            {
                // Deal rest of the cards.
                while (_gameEngine.DealerWantCard(_gameEngine.Dealer.Hand))
                {
                    DrawNewCard(DealerId);
                }

                // Decide winners and loosers
                _gameEngine.Evaluate();
                UpdateMoney();
                btnStartNewGame.Visibility = Visibility.Visible;
                MenuItemAddPlayer.IsEnabled = true;
            }
        }

        /// <summary>
        /// Method for the "Start new game" button.
        /// Removes the cards from the dealers and players hands and puts
        /// them back in the deck.
        /// </summary>
        private void btnStartNewGame_Click(object sender, RoutedEventArgs e)
        {
            // Allowed to add players betwenn each round
            MenuItemAddPlayer.IsEnabled = false;

            // Remove all cards from all hands and add them to the decks
            _gameEngine.ClearCards();
            
            // Remove all images of the cards from the UI
            foreach (Image dealerCardImage in _cardImages)
            {
                MainCanvas.Children.Remove(dealerCardImage);
            }
            _cardImages.Clear();

            // Enable bets for all players
            foreach (PlayerControl playerControl in _playerControls)
            {
                playerControl.SetBetsEnabled(true);
            }

            // Update the scores and money labels
            UpdateScores();
            UpdateMoney();

            // Hide button
            btnStartNewGame.Visibility = Visibility.Hidden;
        }

        /// <summary>
        /// Method for the "Add player" menu item.
        /// Opens the NewPlayerWindow that asks for player name and
        /// starting money and then creates the player with that data.
        /// </summary>
        private void MenuItemAddPlayer_Click(object sender, RoutedEventArgs e)
        {
            NewPlayerWindow newPlayerWindow = new NewPlayerWindow();
            if (newPlayerWindow.ShowDialog() == true)
            {
                AddPlayer(newPlayerWindow.PlayerName, newPlayerWindow.PlayerMoney);
            }
        }

        /// <summary>
        /// Method for the "Shuffle cards" menu item. What it does is 
        /// pretty self-explanatory.
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void MenuItemShuffleCards_Click(object sender, RoutedEventArgs e)
        {
            _gameEngine.Deck.Shuffle();
        }

        /// <summary>
        /// The exit menu item. Closes the application. 
        /// </summary>
        private void MenuItemExit_Click(object sender, RoutedEventArgs e)
        {
            Close();
        }


        /// <summary>
        /// The about menu item. Shows a MessageBox with some simple information.
        /// </summary>
        private void MenuItemAbout_Click(object sender, RoutedEventArgs e)
        {
            MessageBox.Show("Black Jack Game v1.0 by Andreas Andersson");
        }


        /**
        * Below are the different methods for betting that are triggered
        * from the different PlayerControls. 
        */

        private void PlayerBetOne(object sender, RoutedEventArgs e)
        {
            Bet(((PlayerControl)sender).Player, 1);
        }

        private void PlayerBetFive(object sender, RoutedEventArgs e)
        {
            Bet(((PlayerControl)sender).Player, 5);
        }

        private void PlayerBetTen(object sender, RoutedEventArgs e)
        {
            Bet(((PlayerControl)sender).Player, 10);
        }

        private void PlayerBetTwentyFive(object sender, RoutedEventArgs e)
        {
            Bet(((PlayerControl)sender).Player, 25);
        }

        private void MenuItemRemovePlayer_Click(object sender, RoutedEventArgs e)
        {
            RemovePlayerWindow removePlayerWindow = new RemovePlayerWindow(_gameEngine.Players);
            if (removePlayerWindow.ShowDialog() == true)
            {
                RemovePlayer(removePlayerWindow.SelectedPlayer);
            }
        }

    }
}
