﻿/**
  Project: BlackJackGame 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-09-22
*/

using System.Windows;
using System.Windows.Controls;
using BlackJackLogic;

namespace BlackJackGame
{
    /// <summary>
    /// Interaction logic for PlayerControl.xaml
    /// A user control that contains all UI elements for a user,
    /// except the card images.
    /// </summary>
    public partial class PlayerControl : UserControl
    {
        private Player _player = null;
        public Player Player
        {
            get { return _player; }
        }

        public int PlayerId
        {
            get { return _player.PlayerId; }
        }

        /// <summary>
        /// Constructor.
        /// Sets the default state of all elements and sets
        /// the _player variable to the player that this control
        /// belongs to. 
        /// </summary>
        /// <param name="thisPlayer">The player this control represents/belongs to.</param>
        public PlayerControl(Player thisPlayer)
        {
            InitializeComponent();

            _player = thisPlayer;
            
            lblPlayer.Content = _player.PlayerId + " - " + _player.Name;
            btnHitMe.IsEnabled = false;
            btnStand.IsEnabled = false;
            SetBetsEnabled(false);
            UpdateScore();
            UpdateMoney();
        }

        /// <summary>
        /// Updates the bet and money labels for this player.
        /// </summary>
        public void UpdateMoney()
        {
            lblPlayerMoney.Content = _player.Money.ToString() + " SEK";
            lblPlayerBet.Content = _player.Hand.CurrentBet.ToString() + " :-";
        }

        /// <summary>
        /// Updates the score/value label of this player based on the hand.
        /// </summary>
        public void UpdateScore()
        {
            lblPlayerScore.Content = _player.Hand.Score;
        }

        /// <summary>
        /// Enables or disables the betting buttons
        /// </summary>
        /// <param name="enable">True to enable all buttons, false to disable</param>
        public void SetBetsEnabled(bool enable)
        {
            _player.IsDoneBetting = !enable;
            btnBetOne.IsEnabled = enable;
            btnBetFive.IsEnabled = enable;
            btnBetTen.IsEnabled = enable;
            btnBetTwentyFive.IsEnabled = enable;
            btnBetDone.IsEnabled = enable;
        }

        /// <summary>
        /// Used when this player is set as the current player when its this
        /// players turn to hit or stand. It enables the "Hit me" and "Stand" buttons.
        /// </summary>
        public void SetCurrentPlayer()
        {
            btnHitMe.IsEnabled = true;
            btnStand.IsEnabled = true;
        }

        // All the events that is needed between this player and the rest of the application
        public event RoutedEventHandler BetOneEvent;
        public event RoutedEventHandler BetFiveEvent;
        public event RoutedEventHandler BetTenEvent;
        public event RoutedEventHandler BetTwentyFiveEvent;
        public event RoutedEventHandler FinishedBettingEvent;
        public event RoutedEventHandler HitMeEvent;
        public event RoutedEventHandler StandEvent;


        /*
        * Below are a bunch of methods for the different buttons in the UI.
        * They simply trigger a routed event the main UI can subscribe to. 
        * They also enable or disable the relevant elements.
        */

        private void btnBetOne_Click(object sender, RoutedEventArgs e)
        {
            if (BetOneEvent != null) BetOneEvent.Invoke(this, e);
        }

        private void btnBetFive_Click(object sender, RoutedEventArgs e)
        {
            if (BetFiveEvent != null) BetFiveEvent.Invoke(this, e);
        }

        private void btnBetTen_Click(object sender, RoutedEventArgs e)
        {
            if (BetTenEvent != null) BetTenEvent.Invoke(this, e);
        }

        private void btnBetTwentyFive_Click(object sender, RoutedEventArgs e)
        {
            if (BetTwentyFiveEvent != null) BetTwentyFiveEvent.Invoke(this, e);
        }

        private void btnHitMe_Click(object sender, RoutedEventArgs e)
        {
            if (HitMeEvent != null) HitMeEvent.Invoke(this, e);
        }

        private void btnStand_Click(object sender, RoutedEventArgs e)
        {
            btnHitMe.IsEnabled = false;
            btnStand.IsEnabled = false;
            if (StandEvent != null) StandEvent.Invoke(this, e);
        }

        private void btnBetDone_Click(object sender, RoutedEventArgs e)
        {
            _player.IsDoneBetting = true;
            SetBetsEnabled(false);
            if (FinishedBettingEvent != null) FinishedBettingEvent.Invoke(this, e);
        }
    }
}
