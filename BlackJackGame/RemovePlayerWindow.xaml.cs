﻿/**
  Project: BlackJackGame 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-10-04
*/

using System.Collections.Generic;
using System.Windows;
using BlackJackLogic;

namespace BlackJackGame
{
    /// <summary>
    /// Interaction logic for RemovePlayerWindow.xaml
    /// </summary>
    public partial class RemovePlayerWindow : Window
    {
        public Player SelectedPlayer { get; private set; }

        public RemovePlayerWindow(List<Player> playerList)
        {
            InitializeComponent();

            foreach (Player player in playerList)
            {
                lbPlayers.Items.Add(player);
            }
        }

        /// <summary>
        /// Method used when the cancel button is clicked.
        /// Sets DialogResult to false, which closes the window.
        /// </summary>
        private void btnCancel_Click(object sender, RoutedEventArgs e)
        {
            DialogResult = false;
        }

        /// <summary>
        /// Method used when the remove button is clicked. 
        /// Sets the property SelectedPlayer to the player selected
        /// in the ListBox and sets the DialogResult to true,
        /// which closes the window.
        /// </summary>
        private void btnRemove_Click(object sender, RoutedEventArgs e)
        {
            if (lbPlayers.SelectedIndex == -1)
            {
                MessageBox.Show("No player selected.");
                return;
            }

            SelectedPlayer = (Player)lbPlayers.SelectedItems[0];

            DialogResult = true;
        }
    }
}
