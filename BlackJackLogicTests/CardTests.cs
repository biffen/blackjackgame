﻿/**
  Project: BlackJackLogicTests 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-10-19
*/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using BlackJackLogic;

namespace BlackJackLogicTests
{
    [TestClass]
    public class CardTests
    {
        [TestMethod]
        public void CreateCardTest()
        {
            Card.Suits expectedSuite = Card.Suits.Hearts;
            int expectedValue = 5;

            Card card = new Card(Card.Suits.Hearts, 5);

            Assert.AreEqual(expectedSuite, 
                card.Suite, 
                "Incorrect suite on newly created card.");

            Assert.AreEqual(expectedValue, 
                card.Value, 
                "Incorrect value on newly created card.");
        }

        [TestMethod]
        public void CardBlackJackValueTest()
        {
            int expectedValue = 10;
            
            Card card = new Card(Card.Suits.Hearts, 11);

            Assert.AreEqual(expectedValue, 
                card.BlackJackValue, 
                "BlackJackValue incorrect.");
        }

        [TestMethod]
        public void SuiteAndValueStringsTest()
        {
            var expectedValues = new
            {
                ValueString = "Jack",
                SuiteString = "Spades"
            };
            //string expectedValueString = "Jack";
            //string expectedSuiteString = "Spades";

            Card card = new Card(Card.Suits.Spades, 11);

            Assert.AreEqual(expectedValues.ValueString, 
                card.ValueString, 
                "Incorrect value string.");

            Assert.AreEqual(expectedValues.SuiteString, 
                card.SuiteString, 
                "Incorrect suite string.");
        }

    }
}
