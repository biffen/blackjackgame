﻿/**
  Project: BlackJackLogicTests 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-10-19
*/

using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

using BlackJackLogic;

namespace BlackJackLogicTests
{
    [TestClass]
    public class HandTests
    {
        [TestMethod]
        public void CreateHandTest()
        {
            int expectedBet = 0;
            int expectedCards = 0;

            Hand newHand = new Hand();
            
            Assert.AreEqual(expectedBet, 
                newHand.CurrentBet, 
                "Default bet value for new hand incorrect.");

            Assert.AreEqual(expectedCards, 
                newHand.NumberOfCards, 
                "Default number of cards for new hand incorrect.");
        }

        [TestMethod]
        public void AddAndClearCardsTest()
        {
            Hand newHand = new Hand();

            newHand.AddCard(new Card(Card.Suits.Clubs, 1));
            newHand.AddCard(new Card(Card.Suits.Hearts, 7));

            int expectedCards = 2;

            Assert.AreEqual(expectedCards, 
                newHand.NumberOfCards, 
                "Incorrect count of cards returned after adding.");

            newHand.Clear();

            expectedCards = 0;

            Assert.AreEqual(expectedCards, 
                newHand.NumberOfCards, 
                "Incorrect count of cards returned after clearing hand.");
        }

        [TestMethod]
        public void BetAndClearBetTest()
        {
            Hand newHand = new Hand();

            newHand.Bet(10);
            newHand.Bet(2);
            newHand.Bet(5);

            int expectedBet = 17;

            Assert.AreEqual(expectedBet, 
                newHand.CurrentBet, 
                "Incorrect bet amount returned after betting.");

            newHand.Clear();

            expectedBet = 0;

            Assert.AreEqual(expectedBet, 
                newHand.CurrentBet, 
                "Incorrect bet amount returned after clearing hand.");
        }
    }
}
