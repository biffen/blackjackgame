﻿/**
  Project: BlackJackLogic 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-09-22
*/

using System;

namespace BlackJackLogic
{
    /// <summary>
    /// Card class that represents one card in the game.
    /// </summary>
    public class Card
    {
        public enum Suits { Clubs, Diamonds, Hearts, Spades }

        private Suits _suite;

        public Suits Suite
        {
            get { return _suite; }
        }

        public int Value { get; private set; }

        /// <summary>
        /// The value used in blackjack (jack, queen and kings 
        /// are worth 10). Aces are unfortunately always worth 1.
        /// </summary>
        public int BlackJackValue
        {
            get
            {
                return Value > 10 ? 10 : Value;
            }
        }

        /// <summary>
        /// The string representation of the Suite (Clubs, Diamonds,
        /// Hearts or Spades).
        /// </summary>
        public string SuiteString
        {
            get { return _suite.ToString(); }
        }

        /// <summary>
        /// The "true" value of the card in text format.
        /// </summary>
        public string ValueString
        {
            get { return ValueToString(Value); }
        }

        /// <summary>
        /// Constructor. 
        /// Sets the suite and value of the this card.
        /// </summary>
        /// <param name="suite">The suite.</param>
        /// <param name="value">The value as a integer.</param>
        public Card(Suits suite, int value)
        {
            _suite = suite;
            Value = value;
        }

        /// <summary>
        /// Returns the given value as a string.
        /// </summary>
        /// <param name="value">The value as an integer.</param>
        /// <returns>The value as a string.</returns>
        private static string ValueToString(int value)
        {
            switch (value)
            {
                case 1:
                    return "One";
                case 2:
                    return "Two";
                case 3:
                    return "Three";
                case 4:
                    return "Four";
                case 5:
                    return "Five";
                case 6:
                    return "Six";
                case 7:
                    return "Seven";
                case 8:
                    return "Eight";
                case 9:
                    return "Nine";
                case 10:
                    return "Ten";
                case 11:
                    return "Jack";
                case 12:
                    return "Queen";
                case 13:
                    return "King";
            }
            return string.Empty;
        }

    }
}
