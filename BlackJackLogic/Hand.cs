﻿/**
  Project: BlackJackLogic 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-09-22
*/

using System.Collections.Generic;
using System.Linq;

namespace BlackJackLogic
{
    /// <summary>
    /// Class that represents one players hand of cards.
    /// Every hand has a list of cards and the amount the player
    /// has bet on the hand.
    /// </summary>
    public class Hand
    {
        private List<Card> _cards;

        public Card LastCard
        {
            get { return _cards.Last(); }
        }

        public int NumberOfCards
        {
            get { return _cards.Count; }
        }

        public int Score
        {
            get { return CalculateScore(); }
        }

        public int CurrentBet { get; private set; }

        /// <summary>
        /// Constructor that initializes the list of cards.
        /// </summary>
        public Hand()
        {
            _cards = new List<Card>();
        }

        /// <summary>
        /// Adds a new card to the list.
        /// </summary>
        /// <param name="newCard">The card to add.</param>
        public void AddCard(Card newCard)
        {
            _cards.Add(newCard);
        }

        /// <summary>
        /// Adds a bet on this hand.
        /// </summary>
        /// <param name="betValue">The amount to bet.</param>
        public void Bet(int betValue)
        {
            CurrentBet += betValue;
        }

        /// <summary>
        /// Removes the cards from the hand and resets the bet.
        /// </summary>
        public void Clear()
        {
            _cards.Clear();
            CurrentBet = 0;
        }

        /// <summary>
        /// Calculates the current score/value of this hand.
        /// </summary>
        /// <returns>Integer with the value/score.</returns>
        private int CalculateScore()
        {
            return _cards.Sum(card => card.BlackJackValue);
        }

        /// <summary>
        /// Returns the enumerator for the list of cards.
        /// </summary>
        /// <returns>Enumerator for the card list.</returns>
        public IEnumerator<Card> GetEnumerator()
        {
            return _cards.GetEnumerator();
        }
    }
}
