﻿/**
  Project: BlackJackLogic 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-10-04
*/

using System.Collections.Generic;
using System.Diagnostics;
using System.Security.Cryptography;

using BlackJackData;

namespace BlackJackLogic
{
    /// <summary>
    /// GameEngine class that is the "brains of the operation". 
    /// It keeps track of players, the deck and 
    /// </summary>
    public class GameEngine
    {
        private const int DealerStartingCash = 5000;

        private Deck _deck;
        private Player _dealer;
        private List<Player> _players;

        public Player Dealer
        {
            get { return _dealer; }
        }

        public Deck Deck
        {
            get { return _deck; }
        }

        public List<Player> Players
        {
            get { return _players; }
        }

        /// <summary>
        /// Constructor. Initializes the player list.
        /// Also creates the decks needed and the dealer.
        /// </summary>
        /// <param name="numOfDecks">The number of decks that should be used.</param>
        public GameEngine(int numOfDecks)
        {
            _players = new List<Player>();
            LoadPlayers();
            _dealer = new Player("Dealer", 0, DealerStartingCash);
            _deck = new Deck(numOfDecks);
        }

        /// <summary>
        /// Adds a new player with the given name and money.
        /// </summary>
        /// <param name="playerName">The name of the new player.</param>
        /// <param name="startingMoney">The starting money of the new player.</param>
        /// <returns>The created player.</returns>
        public Player AddPlayer(string playerName, int startingMoney)
        {
            // Create a new Player object without an ID
            Player newPlayer = new Player(playerName, startingMoney);

            // Add the player to the list
            _players.Add(newPlayer);

            // Add player to the database
            if(newPlayer.SaveToDb())
                Debug.WriteLine("Player " + newPlayer.Name + " saved to db!");
            else
                Debug.WriteLine("ERROR saving player " + newPlayer.Name + " to db!");

            // Returns the player (which will now have an ID that was assignmed by the db)
            return newPlayer;
        }

        /// <summary>
        /// Removes the given player from the database and the list.
        /// </summary>
        /// <param name="player">The player to remove.</param>
        /// <returns>True if removal was a success, otherwise false.</returns>
        public bool RemovePlayer(Player player)
        {
            if (!PlayerDal.Remove(player.PlayerId))
            {
                Debug.WriteLine("PlayerDal returned false when removing player.");
                return false;
            }
            _players.Remove(player);
            Debug.WriteLine("GameEngine: Player removed.");

            return true;
        }

        /// <summary>
        /// Uses the PlayerDal to load all player stored in the database.
        /// </summary>
        private void LoadPlayers()
        {
            foreach (PlayerModel player in PlayerDal.GetPlayers())
            {
                _players.Add(new Player(player.Name, player.PlayerId, player.Money));
            }
        }

        /// <summary>
        /// Decides whether to stand or hit on the given hand.
        /// </summary>
        /// <param name="hand">The hand to "judge"</param>
        /// <returns>True if hit, false if stand.</returns>
        public bool DealerWantCard(Hand hand)
        {
            if (hand.Score < 17)
            {
                return true;
            }
            return false;
        }

        /// <summary>
        /// Evaluates who wins or loses and deals out the right amount of money.
        /// This is very basic and are most likely missing a lot of cases.
        /// </summary>
        public void Evaluate()
        {
            foreach (Player player in _players)
            {
                if (Dealer.Hand.Score > 21 
                    && player.Hand.Score <= 21)
                {
                    player.AddMoney(player.Hand.CurrentBet * 2);
                }
                else if (Dealer.Hand.Score <= 21 
                    && player.Hand.Score <= 21
                    && player.Hand.Score > Dealer.Hand.Score)
                {
                    player.AddMoney(player.Hand.CurrentBet * 2);
                }
                else if (Dealer.Hand.Score < 21
                         && player.Hand.Score < 21
                         && Dealer.Hand.Score == player.Hand.Score)
                {
                    player.AddMoney(player.Hand.CurrentBet);
                }
                else
                {
                    _dealer.AddMoney(player.Hand.CurrentBet);
                }

                // If the player and dealer have the same total, this is called a "push", and the 
                // player typically does not win or lose money on that hand.
            }

            // Update all players in the database with the new values
            foreach (Player player in _players)
            {
                if(!PlayerDal.Update(player.PlayerId, player.Name, player.Money))
                    Debug.WriteLine("Could not update " + player.Name);
            }
        }

        /// <summary>
        /// Removes all the cards from the dealer and players and puts them back
        /// in the deck.
        /// </summary>
        public void ClearCards()
        {
            using (IEnumerator<Card> cards = _dealer.Hand.GetEnumerator())
            {
                while (cards.MoveNext())
                {
                    _deck.AddCard(cards.Current);
                }
                _dealer.Hand.Clear();
            }

            foreach (Player player in _players)
            {
                using (IEnumerator<Card> cards = player.Hand.GetEnumerator())
                {
                    while (cards.MoveNext())
                    {
                        _deck.AddCard(cards.Current);
                    }
                    player.Hand.Clear();
                }
            }
        }

    }
}
