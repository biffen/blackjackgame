﻿/**
  Project: BlackJackLogic 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-09-22
*/

using System;
using System.Collections.Generic;
using System.Diagnostics;

namespace BlackJackLogic
{
    /// <summary>
    /// Represents a deck or several decks.
    /// The multiplier decides how many of each card (i.e. how many decks)
    /// should be used. 
    /// </summary>
    public class Deck
    {
        private static Random _random = new Random();

        private int _cardMultiplier = 1;
        private List<Card> _cards;

        /// <summary>
        /// Deck constructor that sets the card multiplier
        /// and generates the decks.
        /// </summary>
        /// <param name="cardMultiplier"></param>
        public Deck(int cardMultiplier)
        {
            _cardMultiplier = cardMultiplier;
            GenerateDecks();
        }

        /// <summary>
        /// Returns the first card of the deck, and removes it from the _cards list.
        /// </summary>
        /// <returns>The card from the top of the deck.</returns>
        public Card DrawNextCard()
        {
            if (_cards.Count == 0)
            {
                return null;
            }
            Card newCard = _cards[0];
            _cards.RemoveAt(0);
#if DEBUG
            Debug.WriteLine("DrawNextCard -> Cards in deck:" + _cards.Count);
#endif
            return newCard;
        }

        /// <summary>
        /// Adds a card to the deck.
        /// </summary>
        /// <param name="card">The card to add.</param>
        public void AddCard(Card card)
        {
            _cards.Add(card);
#if DEBUG
            Debug.WriteLine("AddCard -> Cards in deck:" + _cards.Count);
#endif
        }

        /// <summary>
        /// Shuffles the card deck.
        /// </summary>
        public void Shuffle()
        {
            for (int i = 0; i < _cards.Count; i++)
            {
                Card tempCard = _cards[i];
                int randomIndex = _random.Next(i, _cards.Count);
                _cards[i] = _cards[randomIndex];
                _cards[randomIndex] = tempCard;
            }

#if DEBUG
            //Debug.WriteLine("Cards after shuffle:");
            //foreach (Card card in _cards)
            //{
            //    Debug.WriteLine(card.SuiteString + card.ValueString);
            //}
#endif
        }

        /// <summary>
        /// Generates a complete deck and shuffles it.
        /// </summary>
        private void GenerateDecks()
        {
            _cards = new List<Card>();
            for (int deck = 0; deck < _cardMultiplier; deck++)
            {
                for (int i = 0; i < 4; i++)
                {
                    for (int j = 1; j <= 13; j++)
                    {
                        switch (i)
                        {
                            case 0:
                                _cards.Add(new Card(Card.Suits.Clubs, j));
                                break;
                            case 1:
                                _cards.Add(new Card(Card.Suits.Diamonds, j));
                                break;
                            case 2:
                                _cards.Add(new Card(Card.Suits.Hearts, j));
                                break;
                            case 3:
                                _cards.Add(new Card(Card.Suits.Spades, j));
                                break;
                        }

                    }
                }
            }
            
            Shuffle();
#if DEBUG
            Debug.WriteLine("GenerateDecks -> Cards in deck:" + _cards.Count);
#endif
        }
    }
}
