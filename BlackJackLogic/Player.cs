﻿/**
  Project: BlackJackLogic 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-10-04
*/

using BlackJackData;

namespace BlackJackLogic
{
    /// <summary>
    /// Player class that represents one blackjack player.
    /// Every player has a unique ID, a hand and money.
    /// </summary>
    public class Player
    {
        public Hand Hand { get; private set; }
        public string Name { get; private set; }
        public int PlayerId { get; private set; }
        public int Money { get; private set; }
        public bool IsDoneBetting { get; set; }

        /// <summary>
        /// Constructor for Player that sets all member variables.
        /// </summary>
        /// <param name="name">The name of the player.</param>
        /// <param name="id">The ID of the player.</param>
        /// <param name="startingMoney">The starting money of the player.</param>
        public Player(string name, int id, int startingMoney)
        {
            IsDoneBetting = false;
            Name = name;
            Hand = new Hand();
            PlayerId = id;
            Money = startingMoney;
        }

        /// <summary>
        /// Constructor for Player that sets the name and money.
        /// </summary>
        /// <param name="name">The name of the player.</param>
        /// <param name="startingMoney">The amount of money he/she has.</param>
        public Player(string name, int startingMoney)
        {
            IsDoneBetting = false;
            Name = name;
            Hand = new Hand();
            Money = startingMoney;
        }

        /// <summary>
        /// Adds money to this player.
        /// </summary>
        /// <param name="money">The amount of money to add.</param>
        public void AddMoney(int money)
        {
            Money += money;
        }

        /// <summary>
        /// Places a bet for this player. Adds the bet to the hand
        /// and removes the same amount from this player.
        /// </summary>
        /// <param name="betValue">The amount of money to bet.</param>
        public void Bet(int betValue)
        {
            Hand.Bet(betValue);
            Money -= betValue;
        }

        /// <summary>
        /// Saves this player to the database.
        /// </summary>
        /// <returns>True if the player was saved, otherwise false.</returns>
        public bool SaveToDb()
        {
            int newId = PlayerDal.Add(Name, Money);

            if (newId == -1)
                return false;

            PlayerId = newId;

            return true;
        }

        /// <summary>
        /// Returns a nice string with the name and money of the player
        /// </summary>
        /// <returns></returns>
        public override string ToString()
        {
            return Name + " (" + Money + ":-)";
        }
    }
}
