﻿/**
  Project: BlackJackData
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-10-04
*/

using System.Data.Entity;

namespace BlackJackData
{
    /// <summary>
    /// Database context for Players (PlayerModel objects).
    /// </summary>
    public class PlayerContext : DbContext
    {
        public DbSet<PlayerModel> Players { get; set; }
    }
}
