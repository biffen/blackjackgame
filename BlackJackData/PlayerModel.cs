﻿/**
  Project: BlackJackData
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-10-04
*/

using System.ComponentModel.DataAnnotations;

namespace BlackJackData
{
    /// <summary>
    /// Data model / Class used to save player information to database.
    /// </summary>
    public class PlayerModel
    {
        [Key]
        public int PlayerId { get; private set; }

        public string Name { get; set; }

        public int Money { get; set; }

        public PlayerModel()
        {

        }

        public PlayerModel(string name, int money)
        {
            Name = name;
            Money = money;
        }

        public PlayerModel(int id, string name, int money)
        {
            PlayerId = id;
            Name = name;
            Money = money;
        }
    }
}
