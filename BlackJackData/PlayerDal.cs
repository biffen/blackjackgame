﻿/**
  Project: BlackJackData 
  Author:  Andreas Andersson (andreas@biffnet.se)
  Edited:  2015-10-04
*/

using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;

namespace BlackJackData
{
    /// <summary>
    /// Static class that is used to modify the Player database.
    /// </summary>
    public static class PlayerDal
    {

        /// <summary>
        /// Adds a player to the database.
        /// </summary>
        /// <param name="name">The name of the player.</param>
        /// <param name="money">The amount of money the player has.</param>
        /// <returns>The ID given to the player by the database, or -1 if 
        /// the operation failed</returns>
        public static int Add(string name, int money)
        {
            // Create the Model object used to save to the database
            PlayerModel newPlayer = new PlayerModel(name, money);

            // Insert into db
            using (var db = new PlayerContext())
            {
                db.Players.Add(newPlayer);
                if (db.SaveChanges() == 1)
                    return newPlayer.PlayerId;
            }

            // Return -1 if the add operation fails
            return -1;
        }

        public static bool Update(int id, string name, int money)
        {
            // Model object that will be used to update the player
            PlayerModel playerToUpdate;

            // Fetch the player from the database
            using (var db = new PlayerContext())
            {
                playerToUpdate = db.Players.FirstOrDefault(p => p.PlayerId == id);
            }

            // If no object is found, bail out
            if (playerToUpdate == null)
                return false;

            // Update the object
            playerToUpdate.Name = name;
            playerToUpdate.Money = money;

            // Mark the object as modified and write changes to database
            using (var db = new PlayerContext())
            {
                db.Entry(playerToUpdate).State = EntityState.Modified;
                db.SaveChanges();
            }

            // Return true since the operation seems successful
            return true;
        }

        /// <summary>
        /// Removes a player from the database with the given ID,
        /// if one is found.
        /// </summary>
        /// <param name="id">The ID of the player that is to be removed.</param>
        /// <returns>True if a player was removes, otherwise false.</returns>
        public static bool Remove(int id)
        {
            using (var db = new PlayerContext())
            {
                // Fetch the object that should be removed
                PlayerModel player = db.Players.FirstOrDefault(p => p.PlayerId == id);

                // If no object was found, bail out
                if (player == null)
                    return false;

                // Remove the object and save changes
                db.Players.Remove(player);
                db.SaveChanges();

                // Return true since the operation seems to be successful
                return true;
            }
        }

        /// <summary>
        /// Get all players from the database and returns them as a
        /// list of PlayerModel objects.
        /// </summary>
        /// <returns>
        /// All players in a list of PlayerModel objects, or null if
        /// there are no players.
        /// </returns>
        public static List<PlayerModel> GetPlayers()
        {
            // Create a list to store the PlayerModel objects fetched from db
            List<PlayerModel> playerList;

            // Fetch all objects as a list
            using (var db = new PlayerContext())
            {
                playerList = db.Players.ToList();
            }

            return playerList;
        }
    }
}
